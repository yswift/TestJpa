# JPA

JPA 用起来才发现有很多不懂的，所有做了测试，写了这些文档(doc文件夹)，下次再有问题时，可以有查询的地方。
感谢[https://vladmihalcea.com](https://vladmihalcea.com)，上面有很多有用的文档和代码。
我列出的参考资料有很多是这个网站的。

## 我的结论
1. 如果取主表数据时，子表数据一定会同时取出，且主表对应的子表数量不多，可以直接用 `@Embeddable` ，可以减少 One to many 的许多麻烦
2. 级联删除，最好直接在数据库中设置，不用 `CascadeType.REMOVE`，也不用 `orphanRemoval = true`
3. `CascadeType` 主表上只用 `PERSIST` 和 `MERGE`，子表都不用设置
4. 如果在主表端完全控制和子表的关联，希望在主表的关联集合List/Set中删除对象时，也删除数据库中的记录，设置主表的 `mappedBy` 和 `orphanRemoval = true`。还要一个好处是插入子表数据时不用执行数据 update
4. 如果在主表端完全控制和子表的关联，希望在主表的关联集合List/Set中删除对象时，也删除数据库中的记录，设置主表的 `mappedBy` 和 `orphanRemoval = true`。还要一个好处是插入子表数据时不用执行数据 update
4. 如果希望在主表的关联集合List/Set中删除对象时，在数据库中设置被删除对象关联主表的外键为Null，即只断开关联，不删除记录，使用@JoinColumn设置关联字段。
5. 主表属性上的 fetch 是`FetchType.EAGER`还是`FetchType.LAZY`看实际需要
6. 子表属性上的 fetch 建议设为`FetchType.LAZY`
7. many to many 的关联集合要使用 `Set` ,而不能是 `List`
8. 如果用Set，还要有序，可以用 `SortedSet<XXX> xx = new TreeSet<>()` 存储数据，用`@OrderBy`，`@SortNatural`或`@SortComparator`指定比较器。参见`Post`类

## 参考

### JPA
- [JPA 文档](https://www.objectdb.com/api/java/jpa)

### One to many
- [The best way to map a @OneToMany relationship with JPA and Hibernate](https://vladmihalcea.com/the-best-way-to-map-a-onetomany-association-with-jpa-and-hibernate/)
- [ManyToOne JPA and Hibernate association best practices](https://vladmihalcea.com/manytoone-jpa-hibernate/)
- [How does orphanRemoval work with JPA and Hibernate](https://vladmihalcea.com/orphanremoval-jpa-hibernate/)

### Many to many
- [Best way to map the JPA and Hibernate ManyToMany relationship](https://vladmihalcea.com/the-best-way-to-use-the-manytomany-annotation-with-jpa-and-hibernate/)
- [The best way to map a many-to-many association with extra columns when using JPA and Hibernate](https://vladmihalcea.com/the-best-way-to-map-a-many-to-many-association-with-extra-columns-when-using-jpa-and-hibernate/)
- [The best way to fix the Hibernate MultipleBagFetchException](https://vladmihalcea.com/hibernate-multiplebagfetchexception/)

### Cascade Types
- [Overview of JPA/Hibernate Cascade Types](https://www.baeldung.com/jpa-cascade-types)
- [Cascade in JPA and Hibernate](https://techrocking.com/cascade-in-jpa-and-hibernate/)
- [A beginner’s guide to JPA and Hibernate Cascade Types](https://vladmihalcea.com/a-beginners-guide-to-jpa-and-hibernate-cascade-types/)

### 其它
- [How to implement Equals and HashCode for JPA entities](https://vladmihalcea.com/hibernate-facts-equals-and-hashcode/)
- [How to synchronize bidirectional entity associations with JPA and Hibernate](https://vladmihalcea.com/jpa-hibernate-synchronize-bidirectional-entity-associations/)


