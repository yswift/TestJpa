package com.example.testjpa.one2many_4;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TestOne2Many4 {
    final String masterName = "master";
    @Autowired
    Master4Repository master4Repository;
    @Autowired
    Slave4Repository slave4Repository;

    void addMaster() {
        Master4 master4 = new Master4();
        master4.setName(masterName);
        for (int i = 0; i < 5; i++) {
            Slave4 slave4 = new Slave4();
            slave4.setName("slave - " + i);
            master4.addSlave(slave4);
        }
        master4Repository.save(master4);
    }

    @Test
    void insert() {
        addMaster();
    }

    @Test
    void updateMaster() {
        Master4 master4 = master4Repository.findFirstByName(masterName);
        master4.setName("124");
        master4Repository.save(master4);
    }

    @Test
    void deleteMaster() {
        Master4 master4 = master4Repository.findFirstByName(masterName);
        master4Repository.delete(master4);
    }

    @Test
    void deleteSlaveFromMaster() {
        // 删除Slave
        Master4 master4 = master4Repository.findFirstByName(masterName);
        Slave4 slave4 = master4.getSlave4List().get(2);
        master4.removeSlave(slave4);
        master4Repository.save(master4);
        System.out.println(master4);
    }

    @Test
    void deleteSlaveFromMaster_2() {
        Master4 master4 = master4Repository.findFirstByName(masterName);
        Slave4 slave4 = master4.getSlave4List().remove(2);
        master4Repository.save(master4);
        slave4Repository.delete(slave4);
    }

    @Test
    void deleteSlaveFromMaster_3() {
        Master4 master4 = master4Repository.findFirstByName(masterName);
        Slave4 slave4 = master4.getSlave4List().remove(2);
        slave4.setMaster4(null);
        master4Repository.save(master4);
        slave4Repository.delete(slave4);
    }

    @Test
    void deleteSlaveFromMaster_4() {
        Master4 master4 = master4Repository.findFirstByName(masterName);
        Slave4 slave4 = master4.getSlave4List().get(2);
        slave4.setMaster4(null);
        slave4Repository.delete(slave4);
    }

    @Test
    void deleteSlave() {
        Slave4 slave4 = slave4Repository.findFirstByName("slave - 1");
        slave4.setMaster4(null);
        slave4Repository.delete(slave4);
    }

    @Test
    void deleteSlave_2() {
        Slave4 slave4 = slave4Repository.findFirstByName("slave - 2");
        slave4Repository.delete(slave4);
    }

    @Test
    void fetchFromSlave() {
        List<Slave4> slave4List = slave4Repository.findAll();
        for (Slave4 s : slave4List) {
            System.out.println("slave: " + s);
            System.out.println("master id = " + s.getMaster4().getId() + s.getMaster4().getName());
        }
    }

    @Test
    void fetchMaster() {
        List<Master4> master4s = master4Repository.findAll();
        for (Master4 m : master4s) {
            System.out.println("master id = " + m.getId() + m.getName());
            System.out.println("    slave size " + m.getSlave4List().size());
        }
    }

    @Test
    void fetchMaster2() {
        Master4 master4 = master4Repository.findFirstByName(masterName);
        System.out.println("master id = " + master4.getId() + master4.getName());
        System.out.println("    slave size " + master4.getSlave4List().size());
    }

    // 修改子表数据所属的主表
    @Test
    void updateSlaveMaster() {
        List<Master4> master4s = master4Repository.findAll();
        Master4 m1 = master4s.get(master4s.size() - 1);
        System.out.println(m1);

        Slave4 slave4 = slave4Repository.findFirstByName("slave - 2");
        System.out.println(slave4);
        if (slave4.getMaster4().getId() != m1.getId()) {
            System.out.println("不同属，修改");
            slave4.setMaster4(m1);
            slave4Repository.save(slave4);
        }
    }

    // 修改子表数据所属的主表2
    @Test
    void updateSlaveMaster2() {
        Master4 m1 = new Master4();
        m1.setId(1L);

        Slave4 slave4 = slave4Repository.findFirstByName("slave - 2");
        System.out.println(slave4);
        if (slave4.getMaster4().getId() != m1.getId()) {
            System.out.println("不同属，修改");
            slave4.setMaster4(m1);
            slave4.setName("update master id = 1");
            slave4Repository.save(slave4);
        }
    }
}
