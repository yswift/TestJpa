package com.example.testjpa.one2many_2;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestOne2Many2 {
    final String masterName = "master";
    @Autowired
    Master2Repository master2Repository;
    @Autowired
    Slave2Repository slave2Repository;

    void addMaster() {
        Master2 master2 = new Master2();
        master2.setName(masterName);
        for (int i=0; i<5; i++) {
            Slave2 slave2 = new Slave2();
            slave2.setName("slave - " + i);
            slave2.setMaster2(master2);
            master2.getSlave2List().add(slave2);
        }
        master2Repository.save(master2);
    }

    @Test
    void insert() {
        addMaster();
    }

    @Test
    void updateMaster() {
        Master2 master2 = master2Repository.findFirstByName(masterName);
        master2.setName("123");
        master2Repository.save(master2);
    }

    @Test
    void deleteMaster() {
        Master2 master2 = master2Repository.findFirstByName(masterName);
        master2Repository.delete(master2);
    }

    @Test
    void deleteSlaveFromMaster() {
        // 删除Slave
        Master2 master2 = master2Repository.findFirstByName(masterName);
        master2.getSlave2List().remove(2);
        master2Repository.save(master2);
    }

    @Test
    void deleteSlaveFromMaster_2() {
        Master2 master2 = master2Repository.findFirstByName(masterName);
        Slave2 slave2 = master2.getSlave2List().remove(2);
        master2Repository.save(master2);
        slave2Repository.delete(slave2);
    }

    @Test
    void deleteSlaveFromMaster_3() {
        Master2 master2 = master2Repository.findFirstByName(masterName);
        Slave2 slave2 = master2.getSlave2List().remove(2);
        slave2.setMaster2(null);
        master2Repository.save(master2);
        slave2Repository.delete(slave2);
    }

    @Test
    void deleteSlaveFromMaster_4() {
        Master2 master2 = master2Repository.findFirstByName(masterName);
        Slave2 slave2 = master2.getSlave2List().get(2);
        slave2.setMaster2(null);
        slave2Repository.delete(slave2);
    }

    @Test
    void deleteSlave() {
        Slave2 slave2 = slave2Repository.findFirstByName("slave - 1");
        slave2.setMaster2(null);
        slave2Repository.delete(slave2);
    }

    @Test
    void deleteSlave_2() {
        Slave2 slave2 = slave2Repository.findFirstByName("slave - 2");
        slave2Repository.delete(slave2);
    }

}
