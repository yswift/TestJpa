package com.example.testjpa.one2many_1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestOne2Many1 {
    final String masterName = "m1";
    @Autowired
    MasterRepository m1Repository;
    @Autowired
    SlaveRepository s1Repository;

    void addM1() {
        Master master = new Master();
        master.setName(masterName);
        for (int i=0; i<5; i++) {
            Slave slave = new Slave();
            slave.setName("s1 - " + i);
            slave.setMaster(master);
            master.getSlaveList().add(slave);
        }
        m1Repository.save(master);
    }

    @Test
    void insert() {
        addM1();
    }

    @Test
    void updateM1() {
        Master master = m1Repository.findFirstByName(masterName);
        master.setName("123");
        m1Repository.save(master);
    }

    @Test
    void deleteM1() {
        Master master = m1Repository.findFirstByName(masterName);
        m1Repository.delete(master);
    }

    @Test
    void deleteS1FromM1() {
        // 删除S1
        Master master = m1Repository.findFirstByName(masterName);
        master.getSlaveList().remove(2);
        m1Repository.save(master);
    }

    @Test
    void deleteS1FromM1_2() {
        Master master = m1Repository.findFirstByName(masterName);
        Slave slave = master.getSlaveList().remove(2);
        m1Repository.save(master);
        s1Repository.delete(slave);
    }

    @Test
    void deleteS1FromM1_3() {
        Master master = m1Repository.findFirstByName(masterName);
        Slave slave = master.getSlaveList().remove(2);
        slave.setMaster(null);
        m1Repository.save(master);
        s1Repository.delete(slave);
    }

    @Test
    void deleteS1FromM1_4() {
        Master master = m1Repository.findFirstByName(masterName);
        Slave slave = master.getSlaveList().get(2);
        slave.setMaster(null);
        s1Repository.delete(slave);
    }

    @Test
    void deleteS1() {
        Slave slave = s1Repository.findFirstByName("s1 - 1");
        slave.setMaster(null);
        s1Repository.delete(slave);
    }

    @Test
    void deleteS1_2() {
        Slave slave = s1Repository.findFirstByName("s1 - 2");
        s1Repository.delete(slave);
    }

}
