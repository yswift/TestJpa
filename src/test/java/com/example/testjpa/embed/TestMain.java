package com.example.testjpa.embed;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestMain {
    final String mainText = "main";
    @Autowired
    MainRepository mainRepository;

    @Test
    public void insert() {
        Main m = new Main();
        m.setText(mainText);
        Detail d1 = new Detail();
        d1.setName("d1");
        d1.setText("dd1");
        m.getDetailList().add(d1);

        Detail d2 = new Detail();
        d2.setName("d2");
        d2.setText("dd2");
        m.getDetailList().add(d2);
        Detail d3 = new Detail();
        d3.setName("d3");
        d3.setText("dd3");
        m.getDetailList().add(d3);

        Detail d4 = new Detail();
        d4.setName("d4");
        d4.setText("dd4");
        m.getDetailList().add(d4);

        mainRepository.save(m);
    }

    @Test
    public void get() {
        Main m = mainRepository.findFirstByText(mainText);
        System.out.println(m);
    }

    @Test
    public void updateMain() {
        Main m = mainRepository.findFirstByText(mainText);
        m.setText("updated");
        mainRepository.save(m);

        Main m2 = mainRepository.findFirstByText("updated");
        System.out.println(m2);
    }

    @Test
    public void updateDetail() {
        Main m = mainRepository.findFirstByText(mainText);
        Detail d11 = new Detail();
        d11.setName("d11");
        d11.setText("dd11");
        // 替换第0
        m.getDetailList().set(0, d11);
        // 修改第1
        m.getDetailList().get(1).setName("修改第1");
        // 删除第2
        m.getDetailList().remove(2);
        // 增加一个
        Detail d5 = new Detail();
        d5.setName("d5");
        d5.setText("dd5");
        m.getDetailList().add(d5);
        mainRepository.save(m);

        Main m2 = mainRepository.findFirstByText(mainText);
        System.out.println(m2);
    }
}
