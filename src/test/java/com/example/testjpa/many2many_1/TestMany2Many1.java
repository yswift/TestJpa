package com.example.testjpa.many2many_1;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;

@SpringBootTest
public class TestMany2Many1 {
    final String title = "JPA with Hibernate";
    final String tagName = "Java";

    @Autowired
    PostRepository postRepository;
    @Autowired
    TagRepository tagRepository;

    void addData() {
        Tag tag1 = new Tag("Java");
        Tag tag2 = new Tag("Hibernate");

        Post post1 = new Post("JPA with Hibernate");
        post1.addTag(tag1);
        post1.addTag(tag2);
        postRepository.save(post1);
        System.out.println(post1);

        Post post2 = new Post("Native Hibernate");
        // 先保存 post，加入tag后在保存一次
        // 否则：PersistentObjectException: detached entity passed to persist
        postRepository.save(post2);
        post2.addTag(tag2);
        postRepository.save(post2);
    }

    @Test
    void insert() {
        addData();
    }

    @Test
    void get() {
        Post post = postRepository.findFirstByTitle(title);
        System.out.println(post);
    }

    @Test
    void delete() {
        Post post = postRepository.findFirstByTitle(title);
        System.out.println(post);
        postRepository.delete(post);
    }

    @Test
    void deleteTag() {
        Post post = postRepository.findFirstByTitle(title);
        System.out.println(post);
        // 删除第一个tag
        for (Tag t : post.getTags()) {
            post.removeTag(t);
            break;
        }
        postRepository.save(post);
    }

    @Test
    void deleteTag2() {
        // 单向关系 直接删除 一条 tag 记录
        Tag tag = tagRepository.findFirstByName(tagName);
        System.out.println(tag);
        tagRepository.delete(tag);
    }

    @Test
    void deleteTag3() {
        // 双向关系 删除 一条 tag 记录
        Post post = postRepository.findFirstByTitle(title);
        System.out.println(post);
        // 删除第一个tag
        Tag dt = post.getTags().first();
        Set<Post> ps = new HashSet<>(dt.getPosts());
        for (Post p : ps) {
            p.removeTag(dt);
        }
        postRepository.saveAll(ps);
        tagRepository.delete(dt);
    }
}
