package com.example.testjpa.one2many_3;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TestOne2Many3 {
    final String masterName = "master";
    @Autowired
    Master3Repository master3Repository;
    @Autowired
    Slave3Repository slave3Repository;

    void addMaster() {
        Master3 master3 = new Master3();
        master3.setName(masterName);
        for (int i=0; i<5; i++) {
            Slave3 slave3 = new Slave3();
            slave3.setName("slave - " + i);
            slave3.setMaster3(master3);
            master3.getSlave3List().add(slave3);
        }
        master3Repository.save(master3);
    }

    @Test
    void insert() {
        addMaster();
    }

    @Test
    void updateMaster() {
        Master3 master3 = master3Repository.findFirstByName(masterName);
        master3.setName("123");
        master3Repository.save(master3);
    }

    @Test
    void deleteMaster() {
        Master3 master3 = master3Repository.findFirstByName(masterName);
        master3Repository.delete(master3);
    }

    @Test
    void deleteSlaveFromMaster() {
        // 删除Slave
        Master3 master3 = master3Repository.findFirstByName(masterName);
        master3.getSlave3List().remove(2);
        master3Repository.save(master3);
    }

    @Test
    void deleteSlaveFromMaster_2() {
        Master3 master3 = master3Repository.findFirstByName(masterName);
        Slave3 slave3 = master3.getSlave3List().remove(2);
        master3Repository.save(master3);
        slave3Repository.delete(slave3);
    }

    @Test
    void deleteSlaveFromMaster_3() {
        Master3 master3 = master3Repository.findFirstByName(masterName);
        Slave3 slave3 = master3.getSlave3List().remove(2);
        slave3.setMaster3(null);
        master3Repository.save(master3);
        slave3Repository.delete(slave3);
    }

    @Test
    void deleteSlaveFromMaster_4() {
        Master3 master3 = master3Repository.findFirstByName(masterName);
        Slave3 slave3 = master3.getSlave3List().get(2);
        slave3.setMaster3(null);
        slave3Repository.delete(slave3);
    }

    @Test
    void deleteSlave() {
        Slave3 slave3 = slave3Repository.findFirstByName("slave - 1");
        slave3.setMaster3(null);
        slave3Repository.delete(slave3);
    }

    @Test
    void deleteSlave_2() {
        Slave3 slave3 = slave3Repository.findFirstByName("slave - 2");
        slave3Repository.delete(slave3);
    }

}
