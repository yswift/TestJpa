package com.example.testjpa.embed;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class Detail {
    private String name;

    private String text;

    @Override
    public String toString() {
        return "Detail{" +
                "name='" + name + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
