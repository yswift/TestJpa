package com.example.testjpa.embed;

import org.springframework.data.jpa.repository.JpaRepository;

interface MainRepository extends JpaRepository<Main, Long> {
    Main findFirstByText(String text);
}
