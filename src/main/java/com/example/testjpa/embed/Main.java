package com.example.testjpa.embed;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
class Main {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String text;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name = "MainDetail",
            joinColumns = @JoinColumn(name = "MainId")
    )
    private List<Detail> detailList = new ArrayList<>();

    @Override
    public String toString() {
        return "Main{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", detailList=" + detailList +
                '}';
    }
}
