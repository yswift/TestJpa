package com.example.testjpa.many2many_1;

import org.springframework.data.jpa.repository.JpaRepository;

interface PostRepository extends JpaRepository<Post, Long> {
    Post findFirstByTitle(String title);
}
