package com.example.testjpa.many2many_1;

import org.springframework.data.jpa.repository.JpaRepository;

interface TagRepository extends JpaRepository<Tag, Long> {
    Tag findFirstByName(String name);
}
