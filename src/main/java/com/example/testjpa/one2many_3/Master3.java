package com.example.testjpa.one2many_3;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

// 主表
@Entity
@Setter
@Getter
class Master3 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "master3", orphanRemoval = true)
    private List<Slave3> slave3List = new ArrayList<>();
}
