package com.example.testjpa.one2many_3;

import org.springframework.data.jpa.repository.JpaRepository;

interface Master3Repository extends JpaRepository<Master3, Long> {
    Master3 findFirstByName(String name);
}
