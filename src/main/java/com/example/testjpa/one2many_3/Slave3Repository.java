package com.example.testjpa.one2many_3;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

interface Slave3Repository extends JpaRepository<Slave3, Long> {
    List<Slave3> findAllByMaster3Id(long master3Id);

    Slave3 findFirstByName(String name);
}
