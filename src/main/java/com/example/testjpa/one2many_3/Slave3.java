package com.example.testjpa.one2many_3;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

// 从表
@Entity
@Getter
@Setter
public class Slave3 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="Master3Id", referencedColumnName = "Id")
    private Master3 master3;
}
