package com.example.testjpa.one2many_4;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

// 主表
@Entity
@Setter
@Getter
class Master4 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "master4", orphanRemoval = true)
    private List<Slave4> slave4List = new ArrayList<>();

    void addSlave(Slave4 slave4) {
        slave4List.add(slave4);
        slave4.setMaster4(this);
    }

    void removeSlave(Slave4 slave4) {
        slave4List.remove(slave4);
        slave4.setMaster4(null);
    }

    @Override
    public String toString() {
        return "Master4{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", slave4List=" + slave4List +
                '}';
    }
}
