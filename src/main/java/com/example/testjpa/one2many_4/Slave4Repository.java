package com.example.testjpa.one2many_4;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

interface Slave4Repository extends JpaRepository<Slave4, Long> {
    List<Slave4> findAllByMaster4Id(long master4Id);

    Slave4 findFirstByName(String name);
}
