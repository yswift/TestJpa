package com.example.testjpa.one2many_4;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

// 从表
@Entity
@Getter
@Setter
public class Slave4 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="Master4Id", referencedColumnName = "Id")
    private Master4 master4;

    @Override
    public String toString() {
        return "Slave4{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
