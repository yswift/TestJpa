package com.example.testjpa.one2many_4;

import org.springframework.data.jpa.repository.JpaRepository;

interface Master4Repository extends JpaRepository<Master4, Long> {
    Master4 findFirstByName(String name);
}
