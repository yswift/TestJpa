package com.example.testjpa.one2many_2;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

interface Slave2Repository extends JpaRepository<Slave2, Long> {
    List<Slave2> findAllByMaster2Id(long master2Id);

    Slave2 findFirstByName(String name);
}
