package com.example.testjpa.one2many_2;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

// 主表
@Entity
@Setter
@Getter
class Master2 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "master2")
    private List<Slave2> slave2List = new ArrayList<>();
}
