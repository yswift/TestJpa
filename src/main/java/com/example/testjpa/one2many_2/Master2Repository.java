package com.example.testjpa.one2many_2;

import org.springframework.data.jpa.repository.JpaRepository;

interface Master2Repository extends JpaRepository<Master2, Long> {
    Master2 findFirstByName(String name);
}
