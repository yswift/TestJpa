package com.example.testjpa.one2many_2;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

// 从表
@Entity
@Getter
@Setter
class Slave2 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="Master2Id", referencedColumnName = "Id")
    private Master2 master2;
}
