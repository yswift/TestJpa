package com.example.testjpa.one2many_1;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

// 主表
@Entity
@Setter
@Getter
class Master {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "MasterId", referencedColumnName = "Id")
    private List<Slave> slaveList = new ArrayList<>();
}
