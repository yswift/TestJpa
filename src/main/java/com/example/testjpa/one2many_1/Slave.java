package com.example.testjpa.one2many_1;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

// 从表
@Entity
@Getter
@Setter
class Slave {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="MasterId", referencedColumnName = "Id")
    private Master master;
}
