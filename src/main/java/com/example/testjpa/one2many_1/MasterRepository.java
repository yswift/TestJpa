package com.example.testjpa.one2many_1;

import org.springframework.data.jpa.repository.JpaRepository;

interface MasterRepository extends JpaRepository<Master, Long> {
    Master findFirstByName(String name);
}
