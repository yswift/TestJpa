package com.example.testjpa.one2many_1;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

interface SlaveRepository extends JpaRepository<Slave, Long> {
    List<Slave> findAllByMasterId(long masterId);

    Slave findFirstByName(String name);
}
