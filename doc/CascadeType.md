
# CascadeType

## JPA Cascade Type

All JPA-specific cascade operations are represented by the javax.persistence.CascadeType enum containing entries:

- ALL
- PERSIST
- MERGE 
- REMOVE
- REFRESH
- DETACH

## 说明如下：

- ALL 看名字就知道，包括所有的
- PERSIST 当保存主表新数据时，连同子表中的新数据一起保存。 propagates the persist operation from a parent to a child entity.
- MERGE 修改主表数据时，子表数据也跟着修改。 propagates the merge operation from a parent to a child entity.
  > Merge is used to update an existing record in the database with a unique identifier. First, the record is loaded from the database in memory and its properties are changed. when the merge operation is fired, the changes are updated in the database.
- REMOVE 删除主表数据时，把关联的子表数据也删除
  > Remove operation delete the record from the database. It also removes the entity from the persistence context.
- REFRESH 放弃当前的修改，从数据库重新取一份
- DETACH 只在persistent context中删除，不删除数据库
  > The detach operation removes the entity from the persistent context.

## orphanRemoval vs. CascadeType.REMOVE
A very common question is how the orphanRemoval mechanism differs from the CascadeType.REMOVE strategy.

If the orphanRemoval mechanism allows us to trigger a remove operation on the disassociated child entity, the CascadeType.REMOVE strategy propagates the remove operation from the parent to all the child entities.

参见[How does orphanRemoval work with JPA and Hibernate](https://vladmihalcea.com/orphanremoval-jpa-hibernate/)

## 参考资料
[Overview of JPA/Hibernate Cascade Types](https://www.baeldung.com/jpa-cascade-types)
[Cascade in JPA and Hibernate](https://techrocking.com/cascade-in-jpa-and-hibernate/)
[A beginner’s guide to JPA and Hibernate Cascade Types](https://vladmihalcea.com/a-beginners-guide-to-jpa-and-hibernate-cascade-types/)


