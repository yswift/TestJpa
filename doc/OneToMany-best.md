# 最佳 one to many

## 结论
使用one to many关系，最好的设置如下
- 主表属性`@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "master4", orphanRemoval = true)`
- 子表属性`@ManyToOne(fetch = FetchType.LAZY)`，不设置任何`cascade`
- 主表属性上的 fetch是`FetchType.EAGER`还是`FetchType.LAZY`看实际需要
- 子表属性上的 fetch 建议设为`FetchType.LAZY`
- 建议为主表属性添加`addXXX`,`removeXXX`方法，设置关联。

参考
- [A beginner’s guide to JPA and Hibernate Cascade Types](https://vladmihalcea.com/a-beginners-guide-to-jpa-and-hibernate-cascade-types/)
- [The best way to map a @OneToMany relationship with JPA and Hibernate](https://vladmihalcea.com/the-best-way-to-map-a-onetomany-association-with-jpa-and-hibernate/)
- [ManyToOne JPA and Hibernate association best practices](https://vladmihalcea.com/manytoone-jpa-hibernate/)
- [How does orphanRemoval work with JPA and Hibernate](https://vladmihalcea.com/orphanremoval-jpa-hibernate/)
- [The best way to map a many-to-many association with extra columns when using JPA and Hibernate](https://vladmihalcea.com/the-best-way-to-map-a-many-to-many-association-with-extra-columns-when-using-jpa-and-hibernate/)
- [The best way to fix the Hibernate MultipleBagFetchException](https://vladmihalcea.com/hibernate-multiplebagfetchexception/)
- [How to synchronize bidirectional entity associations with JPA and Hibernate](https://vladmihalcea.com/jpa-hibernate-synchronize-bidirectional-entity-associations/)
- [Best way to map the JPA and Hibernate ManyToMany relationship](https://vladmihalcea.com/the-best-way-to-use-the-manytomany-annotation-with-jpa-and-hibernate/)
- []()
- []()

实体类在包 `com.example.testjpa.one2many_4`
```java
class Master4 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "master4", orphanRemoval = true)
    private List<Slave4> slave4List = new ArrayList<>();

    void addSlave(Slave4 slave4) {
        slave4List.add(slave4);
        slave4.setMaster4(this);
    }

    void removeSlave(Slave4 slave4) {
        slave4List.remove(slave4);
        slave4.setMaster4(null);
    }
}
public class Slave4 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="Master4Id", referencedColumnName = "Id")
    private Master4 master4;

}
```

测试代码在`TestOne2Many4`
## 插入数据
```java
void addMaster() {
        Master4 master4 = new Master4();
        master4.setName(masterName);
        for (int i=0; i<5; i++) {
        Slave4 slave4 = new Slave4();
        slave4.setName("slave - " + i);
        master4.addSlave(slave4);
        }
        master4Repository.save(master4);
        }
```
执行结果如下，5条insert
```sql
Hibernate: insert into master4 (id, name) values (null, ?)
Hibernate: insert into slave4 (id, master4id, name) values (null, ?, ?)
Hibernate: insert into slave4 (id, master4id, name) values (null, ?, ?)
Hibernate: insert into slave4 (id, master4id, name) values (null, ?, ?)
Hibernate: insert into slave4 (id, master4id, name) values (null, ?, ?)
Hibernate: insert into slave4 (id, master4id, name) values (null, ?, ?)
```

## 从主表中删除子数据
```java
    void deleteSlaveFromMaster() {
        // 删除Slave
        Master4 master4 = master4Repository.findFirstByName(masterName);
        Slave4 slave4 = master4.getSlave4List().get(2);
        master4.removeSlave(slave4);
        master4Repository.save(master4);
        System.out.println(master4);
    }
```
删除了子表数据
```sql
Hibernate: select master4x0_.id as id1_5_, master4x0_.name as name2_5_ from master4 master4x0_ where master4x0_.name=? limit ?
Hibernate: select slave4list0_.master4id as master3_9_0_, slave4list0_.id as id1_9_0_, slave4list0_.id as id1_9_1_, slave4list0_.master4id as master3_9_1_, slave4list0_.name as name2_9_1_ from slave4 slave4list0_ where slave4list0_.master4id=?
Hibernate: select master4x0_.id as id1_5_1_, master4x0_.name as name2_5_1_, slave4list1_.master4id as master3_9_3_, slave4list1_.id as id1_9_3_, slave4list1_.id as id1_9_0_, slave4list1_.master4id as master3_9_0_, slave4list1_.name as name2_9_0_ from master4 master4x0_ left outer join slave4 slave4list1_ on master4x0_.id=slave4list1_.master4id where master4x0_.id=?
Hibernate: delete from slave4 where id=?
```

## 去掉orphanRemoval，再次执行上面的删除

在Master4中去掉orphanRemoval
```java
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "master4")
    private List<Slave4> slave4List = new ArrayList<>();
```
```java
    void deleteSlaveFromMaster() {
        // 删除Slave
        Master4 master4 = master4Repository.findFirstByName(masterName);
        Slave4 slave4 = master4.getSlave4List().get(2);
        master4.removeSlave(slave4);
        master4Repository.save(master4);
        System.out.println(master4);
    }
```
不执行任何删除操作
```sql
Hibernate: select master4x0_.id as id1_5_, master4x0_.name as name2_5_ from master4 master4x0_ where master4x0_.name=? limit ?
Hibernate: select slave4list0_.master4id as master3_9_0_, slave4list0_.id as id1_9_0_, slave4list0_.id as id1_9_1_, slave4list0_.master4id as master3_9_1_, slave4list0_.name as name2_9_1_ from slave4 slave4list0_ where slave4list0_.master4id=?
Hibernate: select master4x0_.id as id1_5_1_, master4x0_.name as name2_5_1_, slave4list1_.master4id as master3_9_3_, slave4list1_.id as id1_9_3_, slave4list1_.id as id1_9_0_, slave4list1_.master4id as master3_9_0_, slave4list1_.name as name2_9_0_ from master4 master4x0_ left outer join slave4 slave4list1_ on master4x0_.id=slave4list1_.master4id where master4x0_.id=?
```

参见 https://vladmihalcea.com/orphanremoval-jpa-hibernate/

## 主表 LAZY 关联子表
```java
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "master4", orphanRemoval = true)
    private List<Slave4> slave4List = new ArrayList<>();
```
查询代码
```java
    void fetchMaster2() {
        Master4 master4 = master4Repository.findFirstByName(masterName);
        System.out.println("master id = " + master4.getId() + master4.getName());
        System.out.println("    slave size " + master4.getSlave4List().size());
    }
```
结果
```sql
Hibernate: select master4x0_.id as id1_5_, master4x0_.name as name2_5_ from master4 master4x0_ where master4x0_.name=? limit ?
master id = 4master
Hibernate: select slave4list0_.master4id as master3_9_0_, slave4list0_.id as id1_9_0_, slave4list0_.id as id1_9_1_, slave4list0_.master4id as master3_9_1_, slave4list0_.name as name2_9_1_ from slave4 slave4list0_ where slave4list0_.master4id=?
Hibernate: select master4x0_.id as id1_5_0_, master4x0_.name as name2_5_0_ from master4 master4x0_ where master4x0_.id=?
    slave size 4
```

## 主表 EAGER 关联子表
```java
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "master4", orphanRemoval = true)
    private List<Slave4> slave4List = new ArrayList<>();
```
查询代码
```java
    void fetchMaster2() {
        Master4 master4 = master4Repository.findFirstByName(masterName);
        System.out.println("master id = " + master4.getId() + master4.getName());
        System.out.println("    slave size " + master4.getSlave4List().size());
    }
```
结果
```sql
Hibernate: select master4x0_.id as id1_5_, master4x0_.name as name2_5_ from master4 master4x0_ where master4x0_.name=? limit ?
Hibernate: select slave4list0_.master4id as master3_9_0_, slave4list0_.id as id1_9_0_, slave4list0_.id as id1_9_1_, slave4list0_.master4id as master3_9_1_, slave4list0_.name as name2_9_1_ from slave4 slave4list0_ where slave4list0_.master4id=?
master id = 4master
    slave size 4
```

## 子表 lazy 关联主表
```java
@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name="Master4Id", referencedColumnName = "Id")
private Master4 master4;
```
```java
    @Test
    void fetchFromSlave() {
        List<Slave4> slave4List = slave4Repository.findAll();
        for (Slave4 s : slave4List) {
            System.out.println("slave: " + s);
            System.out.println("master id = " + s.getMaster4().getId() + s.getMaster4().getName());
        }
    }
```
结果，在取数据是执行关联查询
```sql
Hibernate: select slave4x0_.id as id1_9_, slave4x0_.master4id as master3_9_, slave4x0_.name as name2_9_ from slave4 slave4x0_
Hibernate: select master4x0_.id as id1_5_0_, master4x0_.name as name2_5_0_, slave4list1_.master4id as master3_9_1_, slave4list1_.id as id1_9_1_, slave4list1_.id as id1_9_2_, slave4list1_.master4id as master3_9_2_, slave4list1_.name as name2_9_2_ from master4 master4x0_ left outer join slave4 slave4list1_ on master4x0_.id=slave4list1_.master4id where master4x0_.id=?
Hibernate: select master4x0_.id as id1_5_0_, master4x0_.name as name2_5_0_, slave4list1_.master4id as master3_9_1_, slave4list1_.id as id1_9_1_, slave4list1_.id as id1_9_2_, slave4list1_.master4id as master3_9_2_, slave4list1_.name as name2_9_2_ from master4 master4x0_ left outer join slave4 slave4list1_ on master4x0_.id=slave4list1_.master4id where master4x0_.id=?
...
Hibernate: select master4x0_.id as id1_5_0_, master4x0_.name as name2_5_0_, slave4list1_.master4id as master3_9_1_, slave4list1_.id as id1_9_1_, slave4list1_.id as id1_9_2_, slave4list1_.master4id as master3_9_2_, slave4list1_.name as name2_9_2_ from master4 master4x0_ left outer join slave4 slave4list1_ on master4x0_.id=slave4list1_.master4id where master4x0_.id=?
```

## 子表 EAGER 关联主表
```java
@ManyToOne(fetch = FetchType.EAGER)
@JoinColumn(name="Master4Id", referencedColumnName = "Id")
private Master4 master4;
```
```java
    @Test
    void fetchFromSlave() {
        List<Slave4> slave4List = slave4Repository.findAll();
        for (Slave4 s : slave4List) {
            System.out.println("slave: " + s);
            System.out.println("master id = " + s.getMaster4().getId() + s.getMaster4().getName());
        }
    }
```
结果，与lazy类似，但先执行关联查询，不管是否取数据
```sql
Hibernate: select slave4x0_.id as id1_9_, slave4x0_.master4id as master3_9_, slave4x0_.name as name2_9_ from slave4 slave4x0_
Hibernate: select master4x0_.id as id1_5_0_, master4x0_.name as name2_5_0_, slave4list1_.master4id as master3_9_1_, slave4list1_.id as id1_9_1_, slave4list1_.id as id1_9_2_, slave4list1_.master4id as master3_9_2_, slave4list1_.name as name2_9_2_ from master4 master4x0_ left outer join slave4 slave4list1_ on master4x0_.id=slave4list1_.master4id where master4x0_.id=?
Hibernate: select master4x0_.id as id1_5_0_, master4x0_.name as name2_5_0_, slave4list1_.master4id as master3_9_1_, slave4list1_.id as id1_9_1_, slave4list1_.id as id1_9_2_, slave4list1_.master4id as master3_9_2_, slave4list1_.name as name2_9_2_ from master4 master4x0_ left outer join slave4 slave4list1_ on master4x0_.id=slave4list1_.master4id where master4x0_.id=?
...
Hibernate: select master4x0_.id as id1_5_0_, master4x0_.name as name2_5_0_, slave4list1_.master4id as master3_9_1_, slave4list1_.id as id1_9_1_, slave4list1_.id as id1_9_2_, slave4list1_.master4id as master3_9_2_, slave4list1_.name as name2_9_2_ from master4 master4x0_ left outer join slave4 slave4list1_ on master4x0_.id=slave4list1_.master4id where master4x0_.id=?
```

## 子表中可以直接所属主表记录
```java
    void updateSlaveMaster2() {
        Master4 m1 = new Master4();
        m1.setId(1L);

        Slave4 slave4 = slave4Repository.findFirstByName("slave - 2");
        System.out.println(slave4);
        if (slave4.getMaster4().getId() != m1.getId()) {
            System.out.println("不同属，修改");
            slave4.setMaster4(m1);
            slave4.setName("update master id = 1");
            slave4Repository.save(slave4);
        }
   }
```
Master4 对象都可以不用从数据库取，设置ID即可
```java
Master4 m1 = new Master4();
m1.setId(1L);
```
结果sql
```sql
Hibernate: select slave4x0_.id as id1_9_, slave4x0_.master4id as master3_9_, slave4x0_.name as name2_9_ from slave4 slave4x0_ where slave4x0_.name=? limit ?
Hibernate: select master4x0_.id as id1_5_0_, master4x0_.name as name2_5_0_ from master4 master4x0_ where master4x0_.id=?
Slave4{id=28, name='slave - 2'}
不同属，修改
Hibernate: select slave4x0_.id as id1_9_0_, slave4x0_.master4id as master3_9_0_, slave4x0_.name as name2_9_0_ from slave4 slave4x0_ where slave4x0_.id=?
Hibernate: select master4x0_.id as id1_5_1_, master4x0_.name as name2_5_1_, slave4list1_.master4id as master3_9_3_, slave4list1_.id as id1_9_3_, slave4list1_.id as id1_9_0_, slave4list1_.master4id as master3_9_0_, slave4list1_.name as name2_9_0_ from master4 master4x0_ left outer join slave4 slave4list1_ on master4x0_.id=slave4list1_.master4id where master4x0_.id=?
Hibernate: select master4x0_.id as id1_5_0_, master4x0_.name as name2_5_0_ from master4 master4x0_ where master4x0_.id=?
Hibernate: update slave4 set master4id=?, name=? where id=?
```