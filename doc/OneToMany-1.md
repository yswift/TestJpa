# one to many 1

实体类在包 `com.example.testjpa.one2many_1`

```java
class Master {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "MasterId", referencedColumnName = "Id")
    private List<Slave> slaveList = new ArrayList<>();
}
class Slave {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="MasterId", referencedColumnName = "Id")
    private Master master;
}
```

## 结论
用上面方法设置的关联
- 插入时，如果主表中有子表数据，要执行2次sql，一次insert，一次update
- 删除时，如果只是从主表的List中remove对象，调用主表的save方法，不调用子表的delete方法，
  则仅仅是把子表的外键设为null，不删除记录 
- 删除时，不管从主表还是子表删除，都会删除相关联的所有记录
- 删除时，如果只想删除子表的一条记录，在删除前要设置关联的主表对象为null

测试代码在`TestOne2Many1`
## 插入数据
```java
    void addM1() {
        Master master = new Master();
        master.setName("m1");
        for (int i=0; i<5; i++) {
            Slave slave = new Slave();
            slave.setName("s1 - " + i);
            slave.setMaster(master);
            master.getSlaveList().add(slave);
        }
        m1Repository.save(master);
    }
```
执行结果如下，执行插入后，又执行了update设置外键
5条insert，5条update
```sql
Hibernate: insert into master (id, name) values (null, ?)
Hibernate: insert into slave (id, master_id, name) values (null, ?, ?)
Hibernate: insert into slave (id, master_id, name) values (null, ?, ?)
Hibernate: insert into slave (id, master_id, name) values (null, ?, ?)
Hibernate: insert into slave (id, master_id, name) values (null, ?, ?)
Hibernate: insert into slave (id, master_id, name) values (null, ?, ?)
Hibernate: update slave set master_id=? where id=?
Hibernate: update slave set master_id=? where id=?
Hibernate: update slave set master_id=? where id=?
Hibernate: update slave set master_id=? where id=?
Hibernate: update slave set master_id=? where id=?
```

## 修改主表数据
```java
    void updateM1() {
        Master master = m1Repository.getOne(1L);
        master.setName("123");
        m1Repository.save(master);
    }
```
3条select，1条update
```sql
Hibernate: select master0_.id as id1_0_, master0_.name as name2_0_ from master master0_ where master0_.name=? limit ?
Hibernate: select slavelist0_.master_id as master_i3_1_0_, slavelist0_.id as id1_1_0_, slavelist0_.id as id1_1_1_, slavelist0_.master_id as master_i3_1_1_, slavelist0_.name as name2_1_1_ from slave slavelist0_ where slavelist0_.master_id=?
Hibernate: select master0_.id as id1_0_1_, master0_.name as name2_0_1_, slavelist1_.master_id as master_i3_1_3_, slavelist1_.id as id1_1_3_, slavelist1_.id as id1_1_0_, slavelist1_.master_id as master_i3_1_0_, slavelist1_.name as name2_1_0_ from master master0_ left outer join slave slavelist1_ on master0_.id=slavelist1_.master_id where master0_.id=?
Hibernate: update master set name=? where id=?
```

## 删除主表数据
```java
    void deleteM1() {
        Master master = m1Repository.findFirstByName(masterName);
        m1Repository.delete(master);
    }
```
删除主表数据，同时删除了子表数据
```sql
Hibernate: select master0_.id as id1_0_, master0_.name as name2_0_ from master master0_ where master0_.name=? limit ?
Hibernate: select slavelist0_.master_id as master_i3_1_0_, slavelist0_.id as id1_1_0_, slavelist0_.id as id1_1_1_, slavelist0_.master_id as master_i3_1_1_, slavelist0_.name as name2_1_1_ from slave slavelist0_ where slavelist0_.master_id=?
Hibernate: select master0_.id as id1_0_0_, master0_.name as name2_0_0_, slavelist1_.master_id as master_i3_1_1_, slavelist1_.id as id1_1_1_, slavelist1_.id as id1_1_2_, slavelist1_.master_id as master_i3_1_2_, slavelist1_.name as name2_1_2_ from master master0_ left outer join slave slavelist1_ on master0_.id=slavelist1_.master_id where master0_.id=?
Hibernate: update slave set master_id=null where master_id=?
Hibernate: delete from slave where id=?
Hibernate: delete from slave where id=?
Hibernate: delete from slave where id=?
Hibernate: delete from slave where id=?
Hibernate: delete from slave where id=?
Hibernate: delete from master where id=?
```

## 从主表中删除子数据
```java
    void deleteS1FromM1() {
        // 删除S1
        Master master = m1Repository.findFirstByName(masterName);
        master.getSlaveList().remove(2);
        m1Repository.save(master);
    }
```
因为没有执行子表的delete方法，仅仅只把子表中的对于记录的 主表id 设为null，删除关联，没有真实删除数据
```sql
Hibernate: select master0_.id as id1_0_, master0_.name as name2_0_ from master master0_ where master0_.name=? limit ?
Hibernate: select slavelist0_.master_id as master_i3_1_0_, slavelist0_.id as id1_1_0_, slavelist0_.id as id1_1_1_, slavelist0_.master_id as master_i3_1_1_, slavelist0_.name as name2_1_1_ from slave slavelist0_ where slavelist0_.master_id=?
Hibernate: select master0_.id as id1_0_1_, master0_.name as name2_0_1_, slavelist1_.master_id as master_i3_1_3_, slavelist1_.id as id1_1_3_, slavelist1_.id as id1_1_0_, slavelist1_.master_id as master_i3_1_0_, slavelist1_.name as name2_1_0_ from master master0_ left outer join slave slavelist1_ on master0_.id=slavelist1_.master_id where master0_.id=?
Hibernate: update slave set master_id=null where master_id=? and id=?
```

## 从主表中删除子数据 2
```java
    void deleteS1FromM1_2() {
        Master master = m1Repository.findFirstByName(masterName);
        Slave slave = master.getSlaveList().remove(2);
        m1Repository.save(master);
        s1Repository.delete(slave);
    }
```
删除了该子表记录，以及关联的主表记录，还删除了主表关联的其他子表记录
```sql
Hibernate: select master0_.id as id1_0_, master0_.name as name2_0_ from master master0_ where master0_.name=? limit ?
Hibernate: select slavelist0_.master_id as master_i3_1_0_, slavelist0_.id as id1_1_0_, slavelist0_.id as id1_1_1_, slavelist0_.master_id as master_i3_1_1_, slavelist0_.name as name2_1_1_ from slave slavelist0_ where slavelist0_.master_id=?
Hibernate: select master0_.id as id1_0_1_, master0_.name as name2_0_1_, slavelist1_.master_id as master_i3_1_3_, slavelist1_.id as id1_1_3_, slavelist1_.id as id1_1_0_, slavelist1_.master_id as master_i3_1_0_, slavelist1_.name as name2_1_0_ from master master0_ left outer join slave slavelist1_ on master0_.id=slavelist1_.master_id where master0_.id=?
Hibernate: update slave set master_id=null where master_id=? and id=?
Hibernate: select slave0_.id as id1_1_0_, slave0_.master_id as master_i3_1_0_, slave0_.name as name2_1_0_ from slave slave0_ where slave0_.id=?
Hibernate: select master0_.id as id1_0_1_, master0_.name as name2_0_1_, slavelist1_.master_id as master_i3_1_3_, slavelist1_.id as id1_1_3_, slavelist1_.id as id1_1_0_, slavelist1_.master_id as master_i3_1_0_, slavelist1_.name as name2_1_0_ from master master0_ left outer join slave slavelist1_ on master0_.id=slavelist1_.master_id where master0_.id=?
Hibernate: update slave set master_id=null where master_id=?
Hibernate: delete from slave where id=?
Hibernate: delete from slave where id=?
Hibernate: delete from slave where id=?
Hibernate: delete from slave where id=?
Hibernate: delete from master where id=?
```

## 从主表中删除子数据 3
```java
    void deleteS1FromM1_3() {
        Master master = m1Repository.findFirstByName(masterName);
        Slave slave = master.getSlaveList().remove(2);
        slave.setMaster(null);
        m1Repository.save(master);
        s1Repository.delete(slave);
    }
```
设置关联的主表为null（`s1.setM1(null)`），只删除一条记录
```sql
Hibernate: select master0_.id as id1_0_, master0_.name as name2_0_ from master master0_ where master0_.name=? limit ?
Hibernate: select slavelist0_.master_id as master_i3_1_0_, slavelist0_.id as id1_1_0_, slavelist0_.id as id1_1_1_, slavelist0_.master_id as master_i3_1_1_, slavelist0_.name as name2_1_1_ from slave slavelist0_ where slavelist0_.master_id=?
Hibernate: select master0_.id as id1_0_1_, master0_.name as name2_0_1_, slavelist1_.master_id as master_i3_1_3_, slavelist1_.id as id1_1_3_, slavelist1_.id as id1_1_0_, slavelist1_.master_id as master_i3_1_0_, slavelist1_.name as name2_1_0_ from master master0_ left outer join slave slavelist1_ on master0_.id=slavelist1_.master_id where master0_.id=?
Hibernate: update slave set master_id=null where master_id=? and id=?
Hibernate: select slave0_.id as id1_1_0_, slave0_.master_id as master_i3_1_0_, slave0_.name as name2_1_0_ from slave slave0_ where slave0_.id=?
Hibernate: delete from slave where id=?
```

## 从主表中删除子数据 4
```java
    void deleteS1FromM1_4() {
        Master master = m1Repository.findFirstByName(masterName);
        Slave slave = master.getSlaveList().get(2);
        slave.setMaster(null);
        s1Repository.delete(slave);
    }
```
可以直接删除子表数据，但删除前要去掉关联`s1.setM1(null)`
```sql
Hibernate: select master0_.id as id1_0_, master0_.name as name2_0_ from master master0_ where master0_.name=? limit ?
Hibernate: select slavelist0_.master_id as master_i3_1_0_, slavelist0_.id as id1_1_0_, slavelist0_.id as id1_1_1_, slavelist0_.master_id as master_i3_1_1_, slavelist0_.name as name2_1_1_ from slave slavelist0_ where slavelist0_.master_id=?
Hibernate: select slave0_.id as id1_1_0_, slave0_.master_id as master_i3_1_0_, slave0_.name as name2_1_0_ from slave slave0_ where slave0_.id=?
Hibernate: delete from slave where id=?
```

## 直接删除子表数据
```java
    void deleteS1() {
        Slave slave = s1Repository.findFirstByName("s1 - 1");
        slave.setMaster(null);
        s1Repository.delete(slave);
    }
```
可以直接删除子表数据，但删除前要去掉关联`s1.setM1(null)`
```sql
Hibernate: select slave0_.id as id1_1_, slave0_.master_id as master_i3_1_, slave0_.name as name2_1_ from slave slave0_ where slave0_.name=? limit ?
Hibernate: select slave0_.id as id1_1_0_, slave0_.master_id as master_i3_1_0_, slave0_.name as name2_1_0_ from slave slave0_ where slave0_.id=?
Hibernate: delete from slave where id=?
```

## 直接删除子表数据，不断开关联
```java
    void deleteS1_2() {
        Slave slave = s1Repository.getOne(12L);
        s1Repository.delete(slave);
    }
```
删除了子表，关联的主表，以及主表关联的子表
```sql
Hibernate: select slave0_.id as id1_1_, slave0_.master_id as master_i3_1_, slave0_.name as name2_1_ from slave slave0_ where slave0_.name=? limit ?
Hibernate: select slave0_.id as id1_1_0_, slave0_.master_id as master_i3_1_0_, slave0_.name as name2_1_0_ from slave slave0_ where slave0_.id=?
Hibernate: select master0_.id as id1_0_0_, master0_.name as name2_0_0_, slavelist1_.master_id as master_i3_1_1_, slavelist1_.id as id1_1_1_, slavelist1_.id as id1_1_2_, slavelist1_.master_id as master_i3_1_2_, slavelist1_.name as name2_1_2_ from master master0_ left outer join slave slavelist1_ on master0_.id=slavelist1_.master_id where master0_.id=?
Hibernate: update slave set master_id=null where master_id=?
Hibernate: delete from slave where id=?
Hibernate: delete from slave where id=?
Hibernate: delete from slave where id=?
Hibernate: delete from slave where id=?
Hibernate: delete from master where id=?
```
