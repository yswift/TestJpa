# one to many 2: 使用mappedBy

实体类在包 `com.example.testjpa.one2many_3`
和one to many 2相比在`@OneToMany`上多加了`orphanRemoval = true`
```java
class Master3 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "master3", orphanRemoval = true)
    private List<Slave3> slave3List = new ArrayList<>();
}
public class Slave3 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="Master3Id", referencedColumnName = "Id")
    private Master3 master3;
}
```

## 结论
mappedBy和orphanRemoval，在一方维护的list中remove掉多方时产生的效果。

| 参数 | 设置 | 不设置 |
| ----  | ---- | ---- |
| mappedBy | 无效果 | update被remove掉的多方的外键为null |
| orphanRemoval | remove掉的数据会被彻底删除 | 不会删除remove掉的数据 |

mappedBy：
1. 只有@OneToOne，@OneToMany，@ManyToMany上才有mappedBy属性，ManyToOne不存在该属性；
2. mappedBy标签一定是定义在被拥有方的（被控方），他指向拥有方；
3. mappedBy的含义，应该理解为，拥有方能够自动维护跟被拥有方的关系，当然，如果从被拥有方，通过手工强行来维护拥有方的关系也是可以做到的；
4. mappedBy跟joinColumn/JoinTable总是处于互斥的一方，可以理解为正是由于拥有方的关联被拥有方的字段存在，拥有方才拥有了被拥有方。mappedBy这方定义JoinColumn/JoinTable总是失效的，不会建立对应的字段或者表。

测试代码在`TestOne2Many3`
## 插入数据
```java
    void addMaster() {
        Master4 master4 = new Master4();
        master4.setName(masterName);
        for (int i=0; i<5; i++) {
            Slave4 slave4 = new Slave4();
            slave4.setName("slave - " + i);
            master4.addSlave(slave4);
        }
        master4Repository.save(master4);
    }
```
执行结果如下，5条insert，与非mappedBy比较，少了update
```sql
Hibernate: insert into master2 (id, name) values (null, ?)
Hibernate: insert into slave2 (id, master2id, name) values (null, ?, ?)
Hibernate: insert into slave2 (id, master2id, name) values (null, ?, ?)
Hibernate: insert into slave2 (id, master2id, name) values (null, ?, ?)
Hibernate: insert into slave2 (id, master2id, name) values (null, ?, ?)
Hibernate: insert into slave2 (id, master2id, name) values (null, ?, ?)
```

## 从主表中删除子数据
```java
    void deleteSlaveFromMaster() {
        // 删除Slave
        Master2 master2 = master2Repository.findFirstByName(masterName);
        master2.getSlave2List().remove(2);
        master2Repository.save(master2);
    }
```
同时删除了主表，子表数据
```sql
Hibernate: select master3x0_.id as id1_2_, master3x0_.name as name2_2_ from master3 master3x0_ where master3x0_.name=? limit ?
Hibernate: select slave3list0_.master3id as master3_5_0_, slave3list0_.id as id1_5_0_, slave3list0_.id as id1_5_1_, slave3list0_.master3id as master3_5_1_, slave3list0_.name as name2_5_1_ from slave3 slave3list0_ where slave3list0_.master3id=?
Hibernate: select master3x0_.id as id1_2_1_, master3x0_.name as name2_2_1_, slave3list1_.master3id as master3_5_3_, slave3list1_.id as id1_5_3_, slave3list1_.id as id1_5_0_, slave3list1_.master3id as master3_5_0_, slave3list1_.name as name2_5_0_ from master3 master3x0_ left outer join slave3 slave3list1_ on master3x0_.id=slave3list1_.master3id where master3x0_.id=?
Hibernate: delete from slave3 where id=?
Hibernate: delete from slave3 where id=?
Hibernate: delete from slave3 where id=?
Hibernate: delete from slave3 where id=?
Hibernate: delete from slave3 where id=?
Hibernate: delete from master3 where id=?
```

## 从主表中删除子数据 3
```java
    void deleteSlaveFromMaster_3() {
        Master2 master2 = master2Repository.findFirstByName(masterName);
        Slave2 slave2 = master2.getSlave2List().remove(2);
        slave2.setMaster2(null);
        master2Repository.save(master2);
        slave2Repository.delete(slave2);
    }
```
删除了所有记录
```sql
Hibernate: select master3x0_.id as id1_2_, master3x0_.name as name2_2_ from master3 master3x0_ where master3x0_.name=? limit ?
Hibernate: select slave3list0_.master3id as master3_5_0_, slave3list0_.id as id1_5_0_, slave3list0_.id as id1_5_1_, slave3list0_.master3id as master3_5_1_, slave3list0_.name as name2_5_1_ from slave3 slave3list0_ where slave3list0_.master3id=?
Hibernate: select master3x0_.id as id1_2_1_, master3x0_.name as name2_2_1_, slave3list1_.master3id as master3_5_3_, slave3list1_.id as id1_5_3_, slave3list1_.id as id1_5_0_, slave3list1_.master3id as master3_5_0_, slave3list1_.name as name2_5_0_ from master3 master3x0_ left outer join slave3 slave3list1_ on master3x0_.id=slave3list1_.master3id where master3x0_.id=?
Hibernate: delete from slave3 where id=?
Hibernate: delete from slave3 where id=?
Hibernate: delete from slave3 where id=?
Hibernate: delete from slave3 where id=?
Hibernate: delete from master3 where id=?
Hibernate: select slave3x0_.id as id1_5_0_, slave3x0_.master3id as master3_5_0_, slave3x0_.name as name2_5_0_ from slave3 slave3x0_ where slave3x0_.id=?
```