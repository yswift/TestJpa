# many to many 单向关联

使用 https://vladmihalcea.com/the-best-way-to-use-the-manytomany-annotation-with-jpa-and-hibernate/ 
提供的many to many关系

## 实体类
```java
class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    public Post() {}

    public Post(String title) {
        this.title = title;
    }

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "PostTag",
            joinColumns = @JoinColumn(name = "PostId"),
            inverseJoinColumns = @JoinColumn(name = "TagId")
    )
//    @SortNatural
//    private SortedSet<Tag> tags = new TreeSet<>();
    private Set<Tag> tags = new HashSet<>();

    //Getters and setters ommitted for brevity

    public void addTag(Tag tag) {
        tags.add(tag);
//        tag.getPosts().add(this);
    }

    public void removeTag(Tag tag) {
        tags.remove(tag);
//        tag.getPosts().remove(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Post)) return false;
        return id != null && id.equals(((Post) o).getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", tags=" + tags +
                '}';
    }
}

class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //    @NaturalId
    private String name;

//    @ManyToMany(mappedBy = "tags")
//    private Set<Post> posts = new HashSet<>();

    public Tag() {}

    public Tag(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return Objects.equals(name, tag.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Tag{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
```

## 插入数据
```java
    void addData() {
        Tag tag1 = new Tag("Java");
        Tag tag2 = new Tag("Hibernate");

        Post post1 = new Post("JPA with Hibernate");
        post1.addTag(tag1);
        post1.addTag(tag2);
        postRepository.save(post1);
        System.out.println(post1);

        Post post2 = new Post("Native Hibernate");
        // 先保存 post，加入tag后在保存一次
        // 否则：PersistentObjectException: detached entity passed to persist
        postRepository.save(post2);
        post2.addTag(tag2);
        postRepository.save(post2);
    }
```

```sql
Hibernate: insert into post (id, title) values (null, ?)
Hibernate: insert into tag (id, name) values (null, ?)
Hibernate: insert into tag (id, name) values (null, ?)
Hibernate: insert into post_tag (post_id, tag_id) values (?, ?)
Hibernate: insert into post_tag (post_id, tag_id) values (?, ?)
Post{id=15, title='JPA with Hibernate', tags=[Tag{id=16, name='Hibernate'}, Tag{id=17, name='Java'}]}
Hibernate: insert into post (id, title) values (null, ?)
Hibernate: select post0_.id as id1_6_1_, post0_.title as title2_6_1_, tags1_.post_id as post_id1_7_3_, tag2_.id as tag_id2_7_3_, tag2_.id as id1_12_0_, tag2_.name as name2_12_0_ from post post0_ left outer join post_tag tags1_ on post0_.id=tags1_.post_id left outer join tag tag2_ on tags1_.tag_id=tag2_.id where post0_.id=?
Hibernate: select tag0_.id as id1_12_0_, tag0_.name as name2_12_0_ from tag tag0_ where tag0_.id=?
Hibernate: insert into post_tag (post_id, tag_id) values (?, ?)
```

## 查询
```java
    void get() {
        Post post = postRepository.findFirstByTitle(title);
        System.out.println(post);
    }
```

```sql
Hibernate: select post0_.id as id1_6_, post0_.title as title2_6_ from post post0_ where post0_.title=? limit ?
Hibernate: select tags0_.post_id as post_id1_7_0_, tags0_.tag_id as tag_id2_7_0_, tag1_.id as id1_12_1_, tag1_.name as name2_12_1_ from post_tag tags0_ inner join tag tag1_ on tags0_.tag_id=tag1_.id where tags0_.post_id=?
Post{id=13, title='JPA with Hibernate', tags=[Tag{id=14, name='Hibernate'}, Tag{id=15, name='Java'}]}
```

## 删除
```java
    void delete() {
        Post post = postRepository.findFirstByTitle(title);
        System.out.println(post);
        postRepository.delete(post);
    }
```
post 实体和关联被删除
```sql
Post{id=13, title='JPA with Hibernate', tags=[Tag{id=14, name='Hibernate'}, Tag{id=15, name='Java'}]}
Hibernate: select post0_.id as id1_6_0_, post0_.title as title2_6_0_ from post post0_ where post0_.id=?
Hibernate: select tag0_.id as id1_12_0_, tag0_.name as name2_12_0_ from tag tag0_ where tag0_.id=?
Hibernate: select tag0_.id as id1_12_0_, tag0_.name as name2_12_0_ from tag tag0_ where tag0_.id=?
Hibernate: select tags0_.post_id as post_id1_7_0_, tags0_.tag_id as tag_id2_7_0_, tag1_.id as id1_12_1_, tag1_.name as name2_12_1_ from post_tag tags0_ inner join tag tag1_ on tags0_.tag_id=tag1_.id where tags0_.post_id=?
Hibernate: delete from post_tag where post_id=?
Hibernate: delete from post where id=?
```

## post中删除tag
```java
    void deleteTag() {
        Post post = postRepository.findFirstByTitle(title);
        System.out.println(post);
        // 删除第一个tag
        for (Tag t : post.getTags()) {
            post.removeTag(t);
            break;
        }
        postRepository.save(post);
    }
```
检查数据库，(post_id=15, tag_id=16)的关联已删除，
```sql
Post{id=15, title='JPA with Hibernate', tags=[Tag{id=16, name='Hibernate'}, Tag{id=17, name='Java'}]}
Hibernate: select post0_.id as id1_6_1_, post0_.title as title2_6_1_, tags1_.post_id as post_id1_7_3_, tag2_.id as tag_id2_7_3_, tag2_.id as id1_12_0_, tag2_.name as name2_12_0_ from post post0_ left outer join post_tag tags1_ on post0_.id=tags1_.post_id left outer join tag tag2_ on tags1_.tag_id=tag2_.id where post0_.id=?
Hibernate: delete from post_tag where post_id=? and tag_id=?
```

## 直接删除一条Tag中的记录
```java
    void deleteTag2() {
        // 直接删除 一条 tag 记录
        Tag tag = tagRepository.findFirstByName(tagName);
        System.out.println(tag);
        tagRepository.delete(tag);
    }
```
删除失败，因主外键约束
```sql
Hibernate: select tag0_.id as id1_12_, tag0_.name as name2_12_ from tag tag0_ where tag0_.name=? limit ?
Tag{id=15, name='Java'}
Hibernate: select tag0_.id as id1_12_0_, tag0_.name as name2_12_0_ from tag tag0_ where tag0_.id=?
Hibernate: delete from tag where id=?

could not execute statement; SQL [n/a]; constraint ["FKAC1WDCHD2PNUR3FL225OBMLG0: PUBLIC.POST_TAG FOREIGN KEY(TAG_ID) REFERENCES PUBLIC.TAG(ID) (17)"; SQL statement:

```

## 在数据库追踪增加级联删除规则后，再次删除
```java
    void deleteTag2() {
        // 直接删除 一条 tag 记录
        Tag tag = tagRepository.findFirstByName(tagName);
        System.out.println(tag);
        tagRepository.delete(tag);
    }
```
删除成功，检查表记录，tag_id = 17 的相关记录被删除
```sql
Hibernate: select tag0_.id as id1_12_, tag0_.name as name2_12_ from tag tag0_ where tag0_.name=? limit ?
Tag{id=17, name='Java'}
Hibernate: select tag0_.id as id1_12_0_, tag0_.name as name2_12_0_ from tag tag0_ where tag0_.id=?
Hibernate: delete from tag where id=?
```

##
```java

```

```sql

```

##
```java

```

```sql

```