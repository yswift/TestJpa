# one to many 2: 使用mappedBy

实体类在包 `com.example.testjpa.one2many_2`

和one to many 1相比在`@OneToMany`上多加了`mappedBy = "master2"`，删除了`@JoinColumn`,
注意：mappedBy = "从类中的实例变量名称"，即：Slave2 类中的Master2类型变量的名称， 不是类名Master2
```java
class Master2 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "master2")
    private List<Slave2> slave2List = new ArrayList<>();
}
class Slave2 {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="Master2Id", referencedColumnName = "Id")
    private Master2 master2;
}
```

## 结论
用上面方法设置的关联
- 插入时，如果主表中有子表数据，只需要执行一次insert，不需要update
- 删除时，如果只是从主表的List中remove对象，调用主表的save方法，不调用子表的delete方法，
  则没有数据被修改
- 删除时，不管从主表还是子表删除，都会删除相关联的所有记录
- 删除时，如果只想删除子表的一条记录，在删除前要设置关联的主表对象为null

测试代码在`TestOne2Many2`
## 插入数据
```java
    void addMaster() {
        Master2 master2 = new Master2();
        master2.setName(masterName);
        for (int i=0; i<5; i++) {
        Slave2 slave2 = new Slave2();
        slave2.setName("slave - " + i);
        slave2.setMaster2(master2);
        master2.getSlave2List().add(slave2);
        }
        master2Repository.save(master2);
    }
```
执行结果如下，5条insert，与非mappedBy比较，少了update
```sql
Hibernate: insert into master2 (id, name) values (null, ?)
Hibernate: insert into slave2 (id, master2id, name) values (null, ?, ?)
Hibernate: insert into slave2 (id, master2id, name) values (null, ?, ?)
Hibernate: insert into slave2 (id, master2id, name) values (null, ?, ?)
Hibernate: insert into slave2 (id, master2id, name) values (null, ?, ?)
Hibernate: insert into slave2 (id, master2id, name) values (null, ?, ?)
```

## 修改主表数据
```java
    void updateMaster() {
        Master2 master2 = master2Repository.findFirstByName(masterName);
        master2.setName("123");
        master2Repository.save(master2);
    }
```
3条select，1条update
```sql
Hibernate: select master2x0_.id as id1_1_, master2x0_.name as name2_1_ from master2 master2x0_ where master2x0_.name=? limit ?
Hibernate: select slave2list0_.master2id as master3_3_0_, slave2list0_.id as id1_3_0_, slave2list0_.id as id1_3_1_, slave2list0_.master2id as master3_3_1_, slave2list0_.name as name2_3_1_ from slave2 slave2list0_ where slave2list0_.master2id=?
Hibernate: select master2x0_.id as id1_1_1_, master2x0_.name as name2_1_1_, slave2list1_.master2id as master3_3_3_, slave2list1_.id as id1_3_3_, slave2list1_.id as id1_3_0_, slave2list1_.master2id as master3_3_0_, slave2list1_.name as name2_3_0_ from master2 master2x0_ left outer join slave2 slave2list1_ on master2x0_.id=slave2list1_.master2id where master2x0_.id=?
Hibernate: update master2 set name=? where id=?
```

## 删除主表数据
```java
    void deleteMaster() {
        Master2 master2 = master2Repository.findFirstByName(masterName);
        master2Repository.delete(master2);
    }
```
删除主表数据，同时删除了子表数据
```sql
Hibernate: select master2x0_.id as id1_1_, master2x0_.name as name2_1_ from master2 master2x0_ where master2x0_.name=? limit ?
Hibernate: select slave2list0_.master2id as master3_3_0_, slave2list0_.id as id1_3_0_, slave2list0_.id as id1_3_1_, slave2list0_.master2id as master3_3_1_, slave2list0_.name as name2_3_1_ from slave2 slave2list0_ where slave2list0_.master2id=?
Hibernate: select master2x0_.id as id1_1_0_, master2x0_.name as name2_1_0_, slave2list1_.master2id as master3_3_1_, slave2list1_.id as id1_3_1_, slave2list1_.id as id1_3_2_, slave2list1_.master2id as master3_3_2_, slave2list1_.name as name2_3_2_ from master2 master2x0_ left outer join slave2 slave2list1_ on master2x0_.id=slave2list1_.master2id where master2x0_.id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from master2 where id=?
```

## 从主表中删除子数据
```java
    void deleteSlaveFromMaster() {
        // 删除Slave
        Master2 master2 = master2Repository.findFirstByName(masterName);
        master2.getSlave2List().remove(2);
        master2Repository.save(master2);
    }
```
没有执行任何修改、删除操作
```sql
Hibernate: select master2x0_.id as id1_1_, master2x0_.name as name2_1_ from master2 master2x0_ where master2x0_.name=? limit ?
Hibernate: select slave2list0_.master2id as master3_3_0_, slave2list0_.id as id1_3_0_, slave2list0_.id as id1_3_1_, slave2list0_.master2id as master3_3_1_, slave2list0_.name as name2_3_1_ from slave2 slave2list0_ where slave2list0_.master2id=?
Hibernate: select master2x0_.id as id1_1_1_, master2x0_.name as name2_1_1_, slave2list1_.master2id as master3_3_3_, slave2list1_.id as id1_3_3_, slave2list1_.id as id1_3_0_, slave2list1_.master2id as master3_3_0_, slave2list1_.name as name2_3_0_ from master2 master2x0_ left outer join slave2 slave2list1_ on master2x0_.id=slave2list1_.master2id where master2x0_.id=?
```

## 从主表中删除子数据 2
```java
    void deleteSlaveFromMaster_2() {
        Master2 master2 = master2Repository.findFirstByName(masterName);
        Slave2 slave2 = master2.getSlave2List().remove(2);
        master2Repository.save(master2);
        slave2Repository.delete(slave2);
    }
```
删除了该子表记录，以及关联的主表记录，还删除了主表关联的其他子表记录
```sql
Hibernate: select master2x0_.id as id1_1_, master2x0_.name as name2_1_ from master2 master2x0_ where master2x0_.name=? limit ?
Hibernate: select slave2list0_.master2id as master3_3_0_, slave2list0_.id as id1_3_0_, slave2list0_.id as id1_3_1_, slave2list0_.master2id as master3_3_1_, slave2list0_.name as name2_3_1_ from slave2 slave2list0_ where slave2list0_.master2id=?
Hibernate: select master2x0_.id as id1_1_1_, master2x0_.name as name2_1_1_, slave2list1_.master2id as master3_3_3_, slave2list1_.id as id1_3_3_, slave2list1_.id as id1_3_0_, slave2list1_.master2id as master3_3_0_, slave2list1_.name as name2_3_0_ from master2 master2x0_ left outer join slave2 slave2list1_ on master2x0_.id=slave2list1_.master2id where master2x0_.id=?
Hibernate: select slave2x0_.id as id1_3_0_, slave2x0_.master2id as master3_3_0_, slave2x0_.name as name2_3_0_ from slave2 slave2x0_ where slave2x0_.id=?
Hibernate: select master2x0_.id as id1_1_1_, master2x0_.name as name2_1_1_, slave2list1_.master2id as master3_3_3_, slave2list1_.id as id1_3_3_, slave2list1_.id as id1_3_0_, slave2list1_.master2id as master3_3_0_, slave2list1_.name as name2_3_0_ from master2 master2x0_ left outer join slave2 slave2list1_ on master2x0_.id=slave2list1_.master2id where master2x0_.id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from master2 where id=?
```

## 从主表中删除子数据 3
```java
    void deleteSlaveFromMaster_3() {
        Master2 master2 = master2Repository.findFirstByName(masterName);
        Slave2 slave2 = master2.getSlave2List().remove(2);
        slave2.setMaster2(null);
        master2Repository.save(master2);
        slave2Repository.delete(slave2);
    }
```
`slave2.setMaster2(null)`设置关联的主表为null，只删除一条记录
```sql
Hibernate: select master2x0_.id as id1_1_, master2x0_.name as name2_1_ from master2 master2x0_ where master2x0_.name=? limit ?
Hibernate: select slave2list0_.master2id as master3_3_0_, slave2list0_.id as id1_3_0_, slave2list0_.id as id1_3_1_, slave2list0_.master2id as master3_3_1_, slave2list0_.name as name2_3_1_ from slave2 slave2list0_ where slave2list0_.master2id=?
Hibernate: select master2x0_.id as id1_1_1_, master2x0_.name as name2_1_1_, slave2list1_.master2id as master3_3_3_, slave2list1_.id as id1_3_3_, slave2list1_.id as id1_3_0_, slave2list1_.master2id as master3_3_0_, slave2list1_.name as name2_3_0_ from master2 master2x0_ left outer join slave2 slave2list1_ on master2x0_.id=slave2list1_.master2id where master2x0_.id=?
Hibernate: select slave2x0_.id as id1_3_0_, slave2x0_.master2id as master3_3_0_, slave2x0_.name as name2_3_0_ from slave2 slave2x0_ where slave2x0_.id=?
Hibernate: delete from slave2 where id=?
```

## 从主表中删除子数据 4
```java
    void deleteSlaveFromMaster_4() {
        Master2 master2 = master2Repository.findFirstByName(masterName);
        Slave2 slave2 = master2.getSlave2List().get(2);
        slave2.setMaster2(null);
        slave2Repository.delete(slave2);
    }
```
可以直接删除子表数据，但删除前要去掉关联`s1.setM1(null)`
```sql
Hibernate: select master2x0_.id as id1_1_, master2x0_.name as name2_1_ from master2 master2x0_ where master2x0_.name=? limit ?
Hibernate: select slave2list0_.master2id as master3_3_0_, slave2list0_.id as id1_3_0_, slave2list0_.id as id1_3_1_, slave2list0_.master2id as master3_3_1_, slave2list0_.name as name2_3_1_ from slave2 slave2list0_ where slave2list0_.master2id=?
Hibernate: select slave2x0_.id as id1_3_0_, slave2x0_.master2id as master3_3_0_, slave2x0_.name as name2_3_0_ from slave2 slave2x0_ where slave2x0_.id=?
Hibernate: delete from slave2 where id=?
```

## 直接删除子表数据
```java
    void deleteS1() {
        Slave slave = s1Repository.findFirstByName("s1 - 1");
        slave.setMaster(null);
        s1Repository.delete(slave);
    }
```
可以直接删除子表数据，但删除前要去掉关联`s1.setM1(null)`
```sql
Hibernate: select slave2x0_.id as id1_3_, slave2x0_.master2id as master3_3_, slave2x0_.name as name2_3_ from slave2 slave2x0_ where slave2x0_.name=? limit ?
Hibernate: select slave2x0_.id as id1_3_0_, slave2x0_.master2id as master3_3_0_, slave2x0_.name as name2_3_0_ from slave2 slave2x0_ where slave2x0_.id=?
Hibernate: delete from slave2 where id=?
```

## 直接删除子表数据，不断开关联
```java
    void deleteSlave_2() {
        Slave2 slave2 = slave2Repository.findFirstByName("slave - 2");
        slave2Repository.delete(slave2);
    }
```
删除了子表，关联的主表，以及主表关联的子表
```sql
Hibernate: select slave2x0_.id as id1_3_, slave2x0_.master2id as master3_3_, slave2x0_.name as name2_3_ from slave2 slave2x0_ where slave2x0_.name=? limit ?
Hibernate: select slave2x0_.id as id1_3_0_, slave2x0_.master2id as master3_3_0_, slave2x0_.name as name2_3_0_ from slave2 slave2x0_ where slave2x0_.id=?
Hibernate: select master2x0_.id as id1_1_0_, master2x0_.name as name2_1_0_, slave2list1_.master2id as master3_3_1_, slave2list1_.id as id1_3_1_, slave2list1_.id as id1_3_2_, slave2list1_.master2id as master3_3_2_, slave2list1_.name as name2_3_2_ from master2 master2x0_ left outer join slave2 slave2list1_ on master2x0_.id=slave2list1_.master2id where master2x0_.id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from slave2 where id=?
Hibernate: delete from master2 where id=?
```